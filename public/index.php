<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->get('/hello', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hola mundo desde GET");
    return $response;
});

$app->post('/hello', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hola mundo desde POST");
    return $response;
});

$app->put('/hello', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hola mundo desde PUT");
    return $response;
});

$app->patch('/hello', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hola mundo desde PATCH");
    return $response;
});

$app->delete('/hello', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Hola mundo desde DELETE");
    return $response;
});

$app->run();